Keine klimaschädlichen Investitionen?
Wird sich Ihre Partei dafür einsetzen, dass die in Punkt 7 beschriebene
Forderung nach Abschaffung städtischer klimaschädlicher Investitionen umgesetzt
wird?

<p><em>Punkt 7 des offenen Briefs:</em>
<strong>Augsburg soll bis Ende 2020 alle Investitionen, die Firmen finanzieren,
deren Geschäftsmodelle auf fossilen Energieträgern wie Öl und Kohle basieren,
abziehen und stattdessen in klimafreundliche Wirtschaftsbereiche
investieren.</strong>
Ferner sollen bis Ende 2020 Anlagerichtlinien für kommunale Finanzrücklagen
erarbeitet bzw. die bestehenden Anlagerichtlinien dahingehend ergänzt werden,
dass klare Ausschlusskriterien für Investitionen in Unternehmen enthalten sind,
deren Geschäftsmodelle auf Kohle und Öl basieren. Augsburg soll die Umsetzung
dieser beiden Forderungen auch in all seinen Beteiligungen mit Nachdruck
anstreben.</p>
