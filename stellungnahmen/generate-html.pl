#!/usr/bin/perl -p

s{^(\s*)__(STYLE|TABLE)__}{
  open my $fh, $2 eq "STYLE" ? "< style.css" : "./generate-table.pl |" or die $!;
  my $data = do { local $/; <$fh> };

  join "\n", map { length $_ ? "$1$_" : "" } split "\n", $data;
}eg;
