#!/usr/bin/env perl

use warnings;
use strict;

sub slurp {
    open my $fh, "<", $_[0] or die $!;
    local $/;
    my $text = <$fh>;
    $text =~ s/\n$//;
    return $text;
}

sub indent {
    my $text = $_[1];
    $text =~ s/\n/\n@{[ " " x $_[0] ]}/g;
    return $text;
}

sub more {
    my ($id, $html) = @_;

    my ($heading, $par) = $html =~ /^<p><em>(Punkt \d+ des offenen Briefs):<\/em>(.*)<\/p>$/s or die;

    return <<EOF;
<input class="more" type="checkbox" id="$id-more">
<p><label class="more" for="$id-more">$heading</label></p>
<div class="more"><p>$par</p></div>
EOF
}

chdir "data" or die $!;

my %partynames =
    ( aib        => "Augsburg in Bürgerhand"
    , gruene     => "Bündnis 90/Die Grünen Augsburg"
    , csu        => "CSU Augsburg"
    , linke      => "DIE LINKE Augsburg"
    , fdp        => "FDP Augsburg"
    , oedp       => "ÖDP Augsburg"
    , "polit-wg" => "Polit-WG"
    , spd        => "SPD Augsburg"
    , "v-partei" => "V-Partei³ Augsburg"
    , wsa        => "WSA"
    , "generation-aux" => "Generation AUX"
    );
my %sizes =
    ( csu   => "4.0em"
    , fdp   => "4.0em"
    , linke => "4.0em"
    , spd   => "4.0em"
    , "v-partei" => "4.0em"
    , wsa => "4.0em"
    , "polit-wg" => "5.0em"
    , "generation-aux" => "5.0em"
    );
my %artikel =
    ( aib    => "von"
    , gruene => "von"
    , csu    => "der"
    , linke  => "von"
    , fdp    => "der"
    , oedp   => "der"
    , "polit-wg" => "der"
    , spd    => "der"
    , "v-partei" => "der"
    , wsa    => "von"
    , "generation-aux" => "von"
    );

my @parties = sort keys %partynames;

print <<EOF;
<script>
  function undo(e) {
    if(window.location.hash === e.href.substr(e.href.indexOf("#"))) {
      event.preventDefault();
      window.location.hash = "#/";
      return false;
    }
    return true;
  }

  document.onkeydown = function (ev) {
    ev = ev || window.event;
    if(ev.key === "Escape" || ev.key == "Esc") {
      window.location.hash = "#/";
    }
  };
</script>

<table>
  <tr class="parties">
    <td></td>
EOF

print "    <td><img src=\"", substr((glob("../$_.*"))[0], 3), "\" alt=\"$partynames{$_}\" title=\"$partynames{$_}\"", $sizes{$_} ? " style=\"width: $sizes{$_}\"" : "", "></td>\n" for @parties;
print "  </tr>\n";

my @blocks;

for my $q (sort glob "*") {
    next unless $q =~ /^\d/;
    next if $q eq "07a" or $q eq "10a" or $q eq "11a";
    my ($heading, $text) = split /\n/, slurp("$q/frage.txt"), 2;
    ($text, my $details) = split /\n\n/, $text;
    $text =~ s/\n/ /g;

    my %answers;
    for my $p (@parties) {
        if(-e "$q/$p.html") {
            $answers{$p} = [ split "\n", slurp("$q/$p.html"), 2 ];
            $answers{$p}[0] =~ s/-plus-leer//;
            $answers{$p}[0] =~ s/-plus//;
        }
    }

    my $qtrimmed = $q;
    $qtrimmed =~ s/^0+//;
    print "  <tr>\n    <th><a href=\"#$q-question\" onclick=\"undo(this)\">$qtrimmed. $heading</a></th>\n";
    for my $p (@parties) {
        print "    <td class=\"dots\">";
        if($answers{$p} and $answers{$p}[0] ne "ja/nein/bessere") {
            print "<a href=\"#$q-$p\" onclick=\"undo(this)\"><img src=\"$answers{$p}[0].svg\" alt=\"$answers{$p}[0]\" title=\"$partynames{$p}: $answers{$p}[0]\"></a>";
        }
        print "</td>\n";
    }
    print "  </tr>\n\n";

    push @blocks, <<EOF;
<div id="$q-question" class="response">
  <a href="#/" class="close">&times;</a>
  <p><strong>Frage $qtrimmed. $text</strong></p>
  @{[ $details ? indent(2, more("$q-question", $details)) : "" ]}
</div>
EOF

    for my $p (@parties) {
        if($answers{$p}) {
            push @blocks, <<EOF;
<div id="$q-$p" class="response">
  <a href="#/" class="close">&times;</a>
  <p><strong>Frage $qtrimmed. $text</strong></p>
  @{[ $details ? indent(2, more("$q-$p-question", $details)) : "" ]}
  <p><strong>Die Position $artikel{$p} $partynames{$p}:</strong></p>

  <p>@{[
    $answers{$p}[0] eq "ja"         ? "Ja." :
    $answers{$p}[0] eq "nein"       ? "Nein." :
    $answers{$p}[0] eq "weder-noch" ? "Aus unserer Sicht gibt es eine bessere Alternative." :
    $answers{$p}[0] eq "none"       ? "Wir beziehen hierzu keine Position." : warn $answers{$p}[0]
  ]}</p>
  @{[ indent(2, $answers{$p}[1]) ]}
</div>

EOF
        }
    }
}

print "</table>\n";

print @blocks;
