#!/usr/bin/env perl

use warnings;
use strict;

use lib glob "/nix/store/*CGI*/lib/perl5/site_perl/5.28.1";
use lib glob "/nix/store/*File-Slurp*/lib/perl5/site_perl/5.28.1";
use lib glob "/nix/store/*HTML-Parser*/lib/perl5/site_perl/5.28.1";

use CGI;
use Digest::SHA qw< sha256_hex >;
use POSIX       qw< strftime >;

my $q = CGI->new();

print $q->header(-type => "text/html; charset=UTF-8");

print <<TMPL;
<!doctype html>
<html lang="de">
<head>
  <title>Augsburg handelt</title>
  <meta name="viewport" content="initial-scale=1">
</head>
<body>
TMPL

my $name = $q->param("name");
unless(defined $name and not $name =~ /[\t\n]/) {
  print "Kein Name angegeben.\n</body></html>\n";
  exit;
}

my $wohnort = $q->param("wohnort");
$wohnort = "" unless defined $wohnort;
exit if $name =~ /[\t\n]/;

my $mail = $q->param("mail");
$mail = "" unless defined $mail;
exit if $mail =~ /[\t\n]/;

my $public = $q->param("public");
unless(defined $public and ($public eq "public" or $public eq "anonymous")) {
  exit;
}

my $date = strftime("%c", localtime);

my $info = "$date\t$name\t$wohnort\t$public\t$mail\n";

open my $fh, ">>", "/home/iblech/ah-supporters.txt" or die $!;
print $fh $info;
close $fh or die $!;

print <<EOF;
Vielen Dank für Ihre Unterstützung. Wir wünschen Ihnen einen schönen Tag
&ndash; bleiben Sie hoffnungsvoll, dass wir es gemeinsam schaffen, das Pariser
Klimaabkommen einzuhalten.<br><br>

<a href="/">Zurück zur Webseite von Augsburg handelt</a>
EOF

print "</body></html>\n";

open $fh, "|-", qw< mail -s AH-Unterstützung iblech@speicherleck.de > or die $!;
print $fh $info or die $!;
close $fh or die $!;
